# Реализация интерфейса Raw Socket через JNI

## Сборка
```
$ mkdir -p build  
$ cd build  
$ cmake ../  
$ make  
```
## Очистка конфигурации
```
$ rm -rf build/
```
## Запуск
```
$ cd build
$ java -jar JNIMain.jar
```
