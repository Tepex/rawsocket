#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>

#include "jni_include/net_tepex_rawsocket_Main.h"

JNIEXPORT jint JNICALL Java_net_tepex_rawsocket_Main_socket(JNIEnv* env, jobject thiz, jint domain, jint type, jint protocol)
{
	int ret = socket(domain, type, protocol);
	if(ret < 0)
	{
		perror("Socket error");
		return NULL;
	}
	return ret;
}

JNIEXPORT void JNICALL Java_net_tepex_rawsocket_Main_close(JNIEnv* env, jobject thiz, jint socket)
{
	close(socket);
	return;
}

JNIEXPORT jint JNICALL Java_net_tepex_rawsocket_Main_shutdown(JNIEnv* env, jobject thiz, jint socket)
{
	jint ret = shutdown(socket, SHUT_RDWR);
	if(ret < 0)
	{
		perror("Shutdown error");
		return NULL;
	}
	return ret;
}

JNIEXPORT jint JNICALL Java_net_tepex_rawsocket_Main_accept(JNIEnv* env, jobject thiz, jint socket, jint port, jobject callback)
{
	struct sockaddr_in sa;
	socklen_t size;
	
	bzero(&sa, sizeof sa);
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	
	int newSocket = accept(socket, (struct sockaddr *)&sa, &size);
	if(newSocket < 0)
	{
		perror("Accept error");
		return NULL;
	}
	
	jclass callbackClass = env->GetObjectClass(callback);
	if(callbackClass == NULL)
	{
		perror("Can't get object class");
		return NULL;
	}
	jmethodID callId = env->GetMethodID(callbackClass, "call", "(II)V");
	if(callId == NULL)
	{
		perror("Can't get callback methodId \"call\"");
		return NULL;
	}
	
	env->CallVoidMethod(callback, callId, ntohl(sa.sin_addr.s_addr), ntohs(sa.sin_port));
	return newSocket;
}

JNIEXPORT jint JNICALL Java_net_tepex_rawsocket_Main_bind(JNIEnv* env, jobject thiz, jint socket, jbyteArray addr, jint port)
{
	struct sockaddr_in sa;

	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	if(addr == NULL) sa.sin_addr.s_addr = htonl(INADDR_ANY);
	
	return bind(socket, (struct sockaddr *)&sa, sizeof sa);
}

JNIEXPORT jint JNICALL Java_net_tepex_rawsocket_Main_listen(JNIEnv* env, jobject thiz, jint socket, jint backlog)
{
	return listen(socket, backlog);
}

JNIEXPORT jint JNICALL Java_net_tepex_rawsocket_Main_connect(JNIEnv* env, jobject thiz, jint socket, jbyteArray addr, jint port)
{
	struct sockaddr_in sa;
	unsigned int a1, a2, a3, a4, mask = 0;
	
	// 0x000000ff
	mask |= (unsigned char)-1;
	
	jbyte* byteAddr = env->GetByteArrayElements(addr, NULL);
	a1 = ((unsigned int)byteAddr[0]) & mask;
	a2 = ((unsigned int)byteAddr[1]) & mask;
	a3 = ((unsigned int)byteAddr[2]) & mask;
	a4 = ((unsigned int)byteAddr[3]) & mask;
	env->ReleaseByteArrayElements(addr, byteAddr, 0);
	printf("addr: %u.%u.%u.%u\n", a1, a2, a3, a4);
	
	sa.sin_family = AF_INET;
	sa.sin_port = htons(port);
	sa.sin_addr.s_addr = htonl((((((a1 << 8) | a2) << 8) | a3) << 8) | a4);
	
	return connect(socket, (struct sockaddr *)&sa, sizeof sa);
}

JNIEXPORT jint JNICALL Java_net_tepex_rawsocket_Main_send(JNIEnv* env, jobject thiz, jint socket, jbyteArray data, jint size, jint flags)
{
	jbyte* byteData = env->GetByteArrayElements(data, NULL);
	int ret = send(socket, byteData, size, flags);
	env->ReleaseByteArrayElements(data, byteData, 0);
	return ret;
}

JNIEXPORT jint JNICALL Java_net_tepex_rawsocket_Main_recv(JNIEnv* env, jobject thiz, jint socket, jint size, jbyteArray buffer, jint flags, jint timeout)
{
	struct timeval tv;
	tv.tv_sec = timeout;
	tv.tv_usec = 0;
	if(setsockopt(socket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv) < 0)
	{
		close(socket);
		perror("Socket setsockopt error");
		return NULL;
	}
	
	jbyte* byteData = env->GetByteArrayElements(buffer, NULL);
	ssize_t ret = recv(socket, byteData, size, flags);
	env->ReleaseByteArrayElements(buffer, byteData, 0);
	return ret;
}

JNIEXPORT jint JNICALL Java_net_tepex_rawsocket_Main_setsockopt(JNIEnv* env, jobject thiz, jint socket, jint level, jint optionName, jobject optionValue)
{
	jclass valueClass = env->GetObjectClass(optionValue);
	if(optionName == SO_REUSEADDR)
	{
		jmethodID intValueId = env->GetMethodID(valueClass, "intValue", "()I");
		if(intValueId == NULL)
		{
			perror("Can't get intValue method");
			return NULL;
		}
		int value = env->CallIntMethod(optionValue, intValueId);
		return setsockopt(socket, level, optionName, &value, sizeof value);
	}
	return NULL;
}
