package net.tepex.rawsocket;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

public class Main
{
	public static void main(String[] args)
	{
		Main main = new Main();
		//if(args.length > 0) main.testConnection(args[0]);
		if(args.length > 0)
		{
			try
			{
				main.echoServer(Integer.parseInt(args[0]));
			}
			catch(NumberFormatException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void testConnection(String addr)
	{
		System.out.println("Connecting "+addr+"...");
		int sock = socket(Types.PF_INET, Types.SOCK_STREAM, 0);
		if(sock == 0) return;
		int ret = connect(sock, new InetSocketAddress(addr, 25));
		if(ret < 0)
		{
			System.err.println("Connection error");
			close(sock);
			return;
		}
		//int n = read
		System.out.println("Connection OK");
		shutdown(sock);
		close(sock);
	}
	
	public void echoServer(int port)
	{
		int socket = socket(Types.PF_INET, Types.SOCK_STREAM, 0);
		if(socket == 0)
		{
			System.err.println("Socket creation error!");
			return;
		}
		if(setsockopt(socket, Types.SOL_SOCKET, Types.SO_REUSEADDR, Types.TRUE) < 0)
		{
			close(socket);
			System.err.println("Socket set options error!");
			return;
		}
		System.out.println("Setsockopt OK. Start binding...");
		if(bind(socket, null, port) < 0)
		{
			close(socket);
			System.err.println("Socket bind error!");
			return;
		}
		System.out.println("Bind OK. Start listen...");
		if(listen(socket, 4) < 0)
		{
			close(socket);
			System.err.println("Socket listen error!");
			return;
		}
		System.out.println("Listen OK. Accepting...");
		int newSocket = accept(socket, port, (clientAddress, clientPort)->
		{
			InetAddress ia = null;
			try
			{
				ia = getInetAddress(clientAddress);
			}
			catch(UnknownHostException nothing) {}
			System.out.println("new socket accepted. Address: "+ia+" port: "+clientPort);
		});
		System.out.println("Connection OK");
		
		byte[] data = GREETING.getBytes();
		if(send(newSocket, data, data.length, 0) < 0)
		{
			close(newSocket);
			System.err.println("Socket send error!");
			return;
		}
		
		byte[] buffer = new byte[3000];
		int size = recv(newSocket, buffer.length, buffer, 0, 5);
		if(size < 0)
		{
			close(newSocket);
			System.err.println("Socket recv error!");
			return;
		}
		String fromClient = new String(buffer, 0, size);
		System.out.println("from client: "+fromClient);
		
		String out = "> "+fromClient;
		byte[] outBytes = out.getBytes();
		send(newSocket, outBytes, outBytes.length, 0);
		try
		{
			Thread.sleep(1000);
		}
		catch(InterruptedException nothing)
		{
		}
		shutdown(newSocket);
		close(newSocket);
	}
	
	public int connect(int socket, InetSocketAddress address)
	{
		return connect(socket, address.getAddress().getAddress(), address.getPort());
	}
	
	public native int accept(int socket, int port, AcceptCallback callback);
	public native int bind(int socket, byte[] address, int port);
	public native int listen(int socket, int backlog);
	public native int connect(int socket, byte[] address, int port);
	public native int socket(int domain, int type, int protocol);
	public native int shutdown(int socket);
	public native void close(int socket);
	public native int setsockopt(int socket, int level, int optionName, Object optionValue);
	public native int recv(int socket, int size, byte[] buffer, int flags, int timeout);
	public native int send(int socket, byte[] buffer, int size, int flags);
	
	public static InetAddress getInetAddress(int packedAddress) throws UnknownHostException
	{
		byte[] bytes = new byte[]
		{
			(byte)((packedAddress >>> 24) & 0xff),
			(byte)((packedAddress >>> 16) & 0xff),
			(byte)((packedAddress >>>  8) & 0xff),
			(byte)((packedAddress       ) & 0xff)
		};
		return InetAddress.getByAddress(bytes);
	}
	
	static
	{
		System.loadLibrary("rawsocket");
	}
	
	public interface AcceptCallback
	{
		public void call(int address, int port);
	}
	
	public static final String GREETING = "Hello from JNI server!\n"; 
}
