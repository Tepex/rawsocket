package net.tepex.rawsocket;

public class Types
{
	public static final int PF_LOCAL = 1;
	public static final int PF_UNIX = PF_LOCAL;
	public static final int PF_INET = 2;
	public static final int PF_ROUTE = 17;
	public static final int PF_KEY = 29;
	public static final int PF_INET6 = 30;
	public static final int PF_SYSTEM = 32;
	public static final int PF_NDRV = 27;
	
	public static final int SOCK_STREAM = 1;
	public static final int SOCK_DGRAM = 2;
	public static final int SOCK_RAW = 3;
	public static final int SOCK_RDM = 4;
	public static final int SOCK_SEQPACKET = 5;
	
	/* setsockopt */
	
	public static final int SOL_SOCKET = 0xffff;
	
	public static final int SO_REUSEADDR = 0x0004;
	public static final int SO_KEEPALIVE = 0x0008;
	public static final int	SO_SNDBUF = 0x1001;		/* send buffer size */
	public static final int	SO_RCVBUF = 0x1002;		/* receive buffer size */
	public static final int	SO_SNDLOWAT = 0x1003;		/* send low-water mark */
	public static final int	SO_RCVLOWAT = 0x1004;		/* receive low-water mark */
	public static final int	SO_SNDTIMEO = 0x1005;		/* send timeout */
	public static final int	SO_RCVTIMEO = 0x1006;		/* receive timeout */
	public static final int	SO_ERROR = 0x1007;		/* get error status and clear */
	public static final int	SO_TYPE = 0x1008;		/* get socket type */
	
	public static final int FALSE = 0;
	public static final int TRUE = 1;
}
