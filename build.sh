#!/bin/sh

if [ "$1" == "clean" ]; then
	echo "Clearing..."
	rm -rf build
	mkdir -p build
	cd build
	cmake ..
else
	cd build
fi
make clean
make
echo "OK"
